#!/usr/bin/env bash
set -e # We want to fail at each command, to stop execution

sudo apt install blueman

sudo add-apt-repository ppa:berglh/pulseaudio-a2dp
sudo apt install pulseaudio-modules-bt libldac

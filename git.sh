#!/usr/bin/env bash
set -e # We want to fail at each command, to stop execution

function message() {
	if ! [[ $2 ]]; then
		# Green bold text
		echo -e '\033[1;32m'$1'\033[0m'
	else
		# Green, red and blue text
		echo -e '\033[1;32m'$1'\033[1;31m >> \033[1;34m'$2'\033[0m'
	fi
}

message 'Installing GIT...'
sudo apt install -y git

read -p "Enter your email: "  email
read -p "Enter your name: "  name

git config --global user.name $name
git config --global user.email $email

git config --global credential.helper store

# Git aliases
message 'Creating GIT aliases...'
git config --global alias.nmerge "merge --no-ff"
git config --global alias.co "checkout"
git config --global alias.br "branch"
git config --global alias.ci "commit"
git config --global alias.st "status"
git config --global alias.unstage "reset HEAD --"
git config --global alias.last "log -1 HEAD"
git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
git config --global alias.uncommit "reset --soft HEAD^"

# GitHub CLI
while true; do
    read -p "Would you like to install GitHub CLI?  [yes/no]" yn
    case $yn in
        [Yy]* )
            message 'Installing GitHub CLI ...'
            sudo deb-get install gh
            message "GitHub CLI  has been installed."
		break
		;;
    esac
done

#  GitHub Desktop
while true; do
    read -p "Would you like to install  GitHub Desktop?  [yes/no]" yn
    case $yn in
        [Yy]* )
            message 'Installing  GitHub Desktop ...'
            sudo deb-get install github-desktop
            message "GitHub Desktop has been installed."
		break
		;;
    esac
done
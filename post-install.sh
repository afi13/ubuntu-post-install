#!/usr/bin/env bash
set -e # We want to fail at each command, to stop execution

#### FUNCTIONS START ####
red='\033[0;31m'
green='\033[0;32m'
NC='\033[0m'

function message() {
	if ! [[ $2 ]]; then
		# Green bold text
		echo -e '\033[1;32m'$1'\033[0m'
	else
		# Green, red and blue text
		echo -e '\033[1;32m'$1'\033[1;31m >> \033[1;34m'$2'\033[0m'
	fi
}

function fancy_message() {
    if [ -z "${1}" ] || [ -z "${2}" ]; then
      return
    fi

    local RED="\e[31m"
    local GREEN="\e[32m"
    local YELLOW="\e[33m"
    local RESET="\e[0m"
    local MESSAGE_TYPE=""
    local MESSAGE=""
    MESSAGE_TYPE="${1}"
    MESSAGE="${2}"

    case ${MESSAGE_TYPE} in
      info) echo -e "  [${GREEN}+${RESET}] ${MESSAGE}";;
      progress) echo -en "  [${GREEN}+${RESET}] ${MESSAGE}";;
      warn) echo -e "  [${YELLOW}*${RESET}] WARNING! ${MESSAGE}";;
      error) echo -e "  [${RED}!${RESET}] ERROR! ${MESSAGE}";;
      fatal) echo -e "  [${RED}!${RESET}] ERROR! ${MESSAGE}"
             exit 1;;
      *) echo -e "  [?] UNKNOWN: ${MESSAGE}";;
    esac
}

function update_apt() {
    sudo apt-get -q -y update
}

function upgrade_apt() {
    sudo apt-get -q -y upgrade
}

function download_deb() {
    local URL="${1}"
    local FILE="${URL##*/}"

    if ! wget --quiet --continue --show-progress --progress=bar:force:noscroll "${URL}" -O "${FILE}"; then
        fancy_message error "Failed to download ${URL}..."
    fi
}

install_deb () {
    local URL="${1}"
    local FILE="${URL##*/}"

    download_deb $URL

    if [ -f ./$FILE ]
        then
        sudo dpkg -i $FILE
        sudo apt-get install -f -y
        sudo apt autoremove
        rm ./$FILE
    fi
}

install_flatpak () {
    sudo apt install -y flatpak
    sudo apt install -y gnome-software-plugin-flatpak

    message 'Installing Flatpak repositories...'
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

install_flatpak_package () {
    local $package="$(basename -- $1)"
    flatpak -y install $package
}

install_snap_package () {
    local $package="$(basename -- $1)"
    sudo snap install $package
}

install_viber () {
    message 'Installing Viber...'
    install_deb https://download.cdn.viber.com/cdn/desktop/Linux/viber.deb
    message "Viber has been installed."
}

install_docksal () {
    echo 'Installing docksal...'
    bash <(curl -fsSL https://get.docksal.io)
    fin config set --global PROJECT_INACTIVITY_TIMEOUT="2h"
    fin system reset
}

install_docker_desktop () {
    message 'Installing Docker Desktop...'
    sudo deb-get install docker-desktop
}

install_docker () {
    message 'Installing Docker...'
    sudo deb-get install docker-ce
    sudo apt-get -y install docker-ce docker-ce-cli docker-compose-plugin
    sudo systemctl enable --now docker
    sudo usermod -aG docker $USER
}

install_jetbrains_toolbox () {
    local FILE="jetbrains-toolbox-1.24.12080.tar.gz"
    local DIR="/opt/jetbrains-toolbox"
    local DEST=$PWD/$FILE

    message 'Downloading Jetbrains Toolbox...'
    wget --quiet --continue --show-progress --progress=bar:force:noscroll https://download.jetbrains.com/toolbox/${FILE}
    if sudo mkdir ${DIR}; then
       sudo tar -xzf "${DEST}" -C ${DIR} --strip-components=1
    fi
    message 'Install Jetbrains Toolbox...'
    sudo chmod -R +rwx ${DIR}
    sudo touch ${DIR}/jetbrains-toolbox.sh
    echo "$DIR/jetbrains-toolbox" | sudo tee -a $DIR/jetbrains-toolbox.sh > /dev/null
    sudo ln -s ${DIR}/jetbrains-toolbox.sh /usr/local/bin/jetbrains-toolbox
    sudo chmod -R +rwx /usr/local/bin/jetbrains-toolbox

    sudo rm "${DEST}"
    message "Jetbrains Toolbox has been installed."
}

install_oh_my_zsh () {
    message 'Installing Oh My Zsh...'
    sudo apt -y install zsh
    sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    message "Oh My Zsh has been installed."
}

install_telegram () {
    message 'Installing Telegram...'
    local FILE="tsetup.tar.xz"
    wget --quiet --continue --show-progress --progress=bar:force:noscroll https://telegram.org/dl/desktop/linux -O ${FILE}

    tar -xJvf ${FILE}
    sudo mv Telegram /opt/telegram
    sudo ln -sf /opt/telegram/Telegram /usr/bin/telegram

    sudo rm ${FILE}
    message "Telegram has been installed."
}

install_bluemail () {
  message 'Installing BlueMail...'
  install_deb https://download.bluemail.me/BlueMail/deb/BlueMail.deb
  message 'BlueMail has been installed ...'
}

install_deb_get () {
  message 'Installing deb-get...'
  curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | sudo -E bash -s install deb-get
  message 'deb-get has been installed...'
}

install_ms_fonts () {
    message 'Accept Microsoft EULA agreement...'
    echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
    sudo apt install -y ttf-mscorefonts-installer
}

function init() {
    # Update and Upgrade
    sudo add-apt-repository -y multiverse

    message 'Updating and Upgrading...'
    update_apt
    upgrade_apt

    sudo apt install -y curl golang-go git

    # Install deb-get
    install_deb_get
    # Accept Microsoft EULA agreement to not block the script execution.
    install_ms_fonts

    # Install drivers
    sudo ubuntu-drivers autoinstall
}

#### FUNCTIONS END ####

init

message 'Installing common apps...'
cat ./config/common.list | while read package
do
   sudo apt install -y $package
done

message 'Installing apps from third-party sources...'
cat ./config/deb_get.list | while read package
do
   sudo deb-get install $package
done

# Flatpak
message 'Installing Flatpak and repositories...'
install_flatpak

message 'Installing flatpak apps...'
cat ./config/flatpak.list | while read package
do
   install_flatpak_package $package
done

message 'Installing snap apps...'
cat ./config/snap.list | while read package
do
   install_snap_package $package
done

# Telegram
install_telegram

# Viber
while true; do
    read -p "Would you like to install Viber?  [yes/no]" yn
    case $yn in
        [Yy]* )
			      install_viber
		break
		;;
    esac
done

while true; do
    read -p "Would you like to install Jetbrains toolbar?  [yes/no]" yn
    case $yn in
        [Yy]* )
            install_jetbrains_toolbox
		break
		;;
    esac
done

# Docker
while true; do
    read -p "Would you like to install Docker and Docker Compose? [yes/no]" yn
    case $yn in
        [Yy]* )
			      install_docker
            install_docker_desktop
            install_docksal
		break
		;;
    esac
done

# Citrix
while true; do
    read -p "Would you like to install Citrix Workspace? [yes/no]" yn
    case $yn in
        [Yy]* )
			      install_deb https://downloads.citrix.com/20976/icaclient_22.5.0.16_amd64.deb
		break
		;;
    esac
done

# SSH
while true; do
    read -p "Would you like to configure SSH keys?" yn
    case $yn in
        [Yy]* )
			  bash tools/ssh.sh
		break
		;;
    esac
done

# Configure GIT.
while true; do
    read -p "Do you wish to install and configure GIT?" yn
    case $yn in
        [Yy]* )
			   bash git.sh
		break
		;;
    esac
done

gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'

# Install Oh My Zsh
install_oh_my_zsh

# Clear apt cache to free up space
message 'Cleanup apt cache...'
sudo apt autoremove
sudo du -sh /var/cache/apt/archives
sudo apt clean

# purge thumbnails cache
message 'Purge thumbnails cache...'
[ -d $HOME/.thumbnails ] && rm -r $HOME/.thumbnails/*
[ -d $HOME/.cache/thumbnails ] && rm -r $HOME/.cache/thumbnails/*

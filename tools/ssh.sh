#!/usr/bin/env bash
set -e # We want to fail at each command, to stop execution

function message() {
	if ! [[ $2 ]]; then
		# Green bold text
		echo -e '\033[1;32m'$1'\033[0m'
	else
		# Green, red and blue text
		echo -e '\033[1;32m'$1'\033[1;31m >> \033[1;34m'$2'\033[0m'
	fi
}

while true; do
    read -p "Do you wish to generate SSH key?" yn
    case $yn in
        [Yy]* )
          read -p "Enter your email: "  email
          ssh-keygen -t rsa -b 4096 -C $email
		break
		;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done

while true; do
    read -p "Would you like to add existing SSH keys?" yn
    case $yn in
        [Yy]* )

          read -rsp $'Press opy your keys to .ssh folder and press any key to continue...\n' -n1 key
          NOT_KEYS_FILES=(authorized_keys known_hosts config)
          KEY_EXTENSIONS=(pub pem ppk)

          find ~/.ssh -type f -exec chmod 600 {} \;
          find ~/.ssh -name "*.pub" -exec chmod 644 {} \;

          message 'Starting the ssh-agent'
          eval "$(ssh-agent -s)"

          for entry in ~/.ssh/*
            do
              if [[ ! " ${NOT_KEYS_FILES[@]} " =~ " basename -- $entry" ]]; then
                extension="${entry##*.}"
                if [[ ! " ${KEY_EXTENSIONS[@]} " =~ " ${extension} " ]]
                then
                  ssh-add $entry
                  message "Added ${entry}"
                fi
              fi
          done
		break
		;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done